import os
from subprocess import call

dirpath, filename = os.path.split(os.path.abspath(__file__))
py_files = [f for f in os.listdir(dirpath) if f.endswith('.py')]

for f in py_files:
    if f != filename:
        call(["python", os.path.join(dirpath, f)])
